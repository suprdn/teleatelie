from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from cabinet import consts


class Customer(models.Model):
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
    )
    date_birth = models.DateField(
        verbose_name='Дата рождения',
        null=True, blank=True,
    )

    class Meta:
        verbose_name = 'Заказчик'

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'


class ManagerPost(models.Model):
    name = models.CharField(
        verbose_name='Наименование должности',
        max_length=64,
    )

    class Meta:
        verbose_name = 'Должность менеджера'

    def __str__(self):
        return self.name


class WorkerPost(models.Model):
    name = models.CharField(
        verbose_name='Наименование должности',
        max_length=64,
    )

    class Meta:
        verbose_name = 'Должность мастера'

    def __str__(self):
        return self.name


class Manager(models.Model):
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
    )
    post = models.ForeignKey(
        to=ManagerPost,
        verbose_name='Должность',
        on_delete=models.SET_NULL,
        null=True, blank=True,
    )

    class Meta:
        verbose_name = 'Менеджер'

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'


class Worker(models.Model):
    first_name = models.CharField(
        verbose_name='Имя',
        max_length=64,
    )
    last_name = models.CharField(
        verbose_name='Фамилия',
        max_length=64,
    )
    post = models.ForeignKey(
        to=WorkerPost,
        on_delete=models.SET_NULL,
        null=True, blank=True,
    )

    class Meta:
        verbose_name = 'Мастер'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class TvBrand(models.Model):
    name = models.CharField(
        verbose_name='Наименование марки',
        max_length=128,
    )

    class Meta:
        verbose_name = 'Марка телевизора'

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(
        verbose_name='Наименование услуги',
        max_length=128,
    )

    class Meta:
        verbose_name = 'Услуга'

    def __str__(self):
        return self.name


class ServicePrice(models.Model):
    service = models.ForeignKey(
        to=Service,
        on_delete=models.CASCADE,
        verbose_name='Услуга'
    )
    price = models.IntegerField(verbose_name='Цена')

    class Meta:
        verbose_name = 'Прейскурант услуг'


class Material(models.Model):
    name = models.CharField(
        verbose_name='Наименование материала',
        max_length=128,
    )

    class Meta:
        verbose_name = 'Материал'

    def __str__(self):
        return self.name


class MaterialPrice(models.Model):
    material = models.ForeignKey(
        to=Material,
        on_delete=models.CASCADE,
        verbose_name='Материал'
    )
    price = models.IntegerField(verbose_name='Цена')

    class Meta:
        verbose_name = 'Прейскурант материалов'


class CustomerRequest(models.Model):
    customer = models.ForeignKey(
        to=Customer,
        on_delete=models.CASCADE,
        verbose_name='Заказчик',
    )
    tv_brand = models.ForeignKey(
        to=TvBrand,
        on_delete=models.SET_NULL,
        verbose_name='Марка телевизора',
        null=True, blank=True,
    )
    description = models.CharField(
        verbose_name='Описание неисправности',
        max_length=512,
    )
    created_at = models.DateField(
        verbose_name='Дата создания',
        default=timezone.now,
    )

    class Meta:
        verbose_name = 'Заявка на ремонт'


class Order(models.Model):
    customer_request = models.OneToOneField(
        to=CustomerRequest,
        on_delete=models.CASCADE,
        verbose_name='Заявка',
    )
    manager = models.ForeignKey(
        to=Manager,
        on_delete=models.RESTRICT,
        verbose_name='Менеджер',
    )
    master = models.ForeignKey(
        to=Worker,
        on_delete=models.SET_NULL,
        verbose_name='Мастер',
        null=True, blank=True,
    )
    materials_need = models.ManyToManyField(
        to=MaterialPrice,
        verbose_name='Материалы',
    )
    service_need = models.ManyToManyField(
        to=ServicePrice,
        verbose_name='Услуги',
    )
    created_at = models.DateField(
        verbose_name='Дата создания',
        default=timezone.now,
    )
    status_choices = [
        ('NEW', consts.STATUS_NEW),
        ('IN_PROGRESS', consts.STATUS_IN_PROGRESS),
        ('DONE', consts.STATUS_DONE),
        ('CLOSED', consts.STATUS_CLOSED),
    ]
    status = models.CharField(
        verbose_name='Статус',
        max_length=64,
        choices=status_choices,
    )

    class Meta:
        verbose_name = 'Заказ'