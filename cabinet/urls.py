from django.urls import path, re_path
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    path('', RedirectView.as_view(url='main/', permanent=True)),
    re_path(r'main/$', views.index, name='index'),
]