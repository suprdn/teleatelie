from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from cabinet.forms import RegisterUserForm
from cabinet.models import Customer, CustomerRequest


def index(request):
    if request.user.is_anonymous:
        return HttpResponseRedirect(reverse('login'))
    return render(
        request,
        'index.html',
    )


def register(request):
    if request.method == 'POST':
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.username = form.cleaned_data.get('username')
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user_password = form.cleaned_data.get('password1')
            user.set_password(user_password)
            user.save()
            customer = Customer(user=user)
            customer.save()
            user = authenticate(username=user.username, password=user_password)
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
    else:
        form = RegisterUserForm()
    return render(
        request,
        '../templates/registration/register.html',
        {'form': form},
    )


class CustomerRequestListView(generic.ListView):
    model = CustomerRequest
    context_object_name = 'customer_request_list'
    template_name = 'customer_request.html'

    def get_queryset(self):
        customer_list = CustomerRequest.objects.filter(customer__user_id=self.request.user.id)
        return customer_list
