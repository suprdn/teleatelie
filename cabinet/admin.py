from django.contrib import admin
from cabinet import models

# Register your models here.
admin.site.register(models.Manager)
admin.site.register(models.Customer)
admin.site.register(models.ManagerPost)
admin.site.register(models.WorkerPost)
admin.site.register(models.Worker)
admin.site.register(models.TvBrand)
admin.site.register(models.Service)
admin.site.register(models.ServicePrice)
admin.site.register(models.Material)
admin.site.register(models.MaterialPrice)
admin.site.register(models.CustomerRequest)
admin.site.register(models.Order)